import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatDialogModule, MatGridListModule} from "@angular/material";
import {PhotoComponent} from './components/photo/photo.component';
import {AlbumComponent} from './components/album/album.component';
import {HttpClientModule} from "@angular/common/http";
import {OpenedAlbumDialogComponent} from './components/opened-album-dialog/opened-album-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    PhotoComponent,
    AlbumComponent,
    OpenedAlbumDialogComponent
  ],
  imports: [
    HttpClientModule,
    MatDialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [OpenedAlbumDialogComponent]
})
export class AppModule {
}
