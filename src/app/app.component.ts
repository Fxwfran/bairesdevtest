import {Component, OnInit} from '@angular/core';
import {ApiService, Photo} from "./services/api.service";

interface Album {
  id
  color
  photos
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  photosArray: Photo[];
  albumsArray: Album[];

  constructor(private apiService: ApiService) {
  }

  async ngOnInit() {
    this.photosArray = await this.apiService.listPhotos().toPromise();
    let lastAlbumID = Math.max(...this.photosArray.map(photo => photo.albumId));
    this.albumsArray = [
      {
        id: lastAlbumID,
        color: '#00ff00',
        photos: this.photosArray.filter(photo => photo.albumId == lastAlbumID).slice(-2)
      },
      {
        id: lastAlbumID - 1,
        color: '#0000ff',
        photos: this.photosArray.filter(photo => photo.albumId == lastAlbumID - 1).slice(-2)
      },
      {
        id: lastAlbumID - 2,
        color: '#660066',
        photos: this.photosArray.filter(photo => photo.albumId == lastAlbumID - 2).slice(-2)
      }]
  }
}
