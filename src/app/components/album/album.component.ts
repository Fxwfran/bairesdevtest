import {Component, Input, OnInit} from '@angular/core';
import {Photo} from "../../services/api.service";
import {MatDialog} from "@angular/material";
import {OpenedAlbumDialogComponent} from "../opened-album-dialog/opened-album-dialog.component";

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  @Input() album: {
    id: number
    color: string
    photos: Photo[]
  };

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(OpenedAlbumDialogComponent, {
      width: '800px',
      data: {albumId: this.album.id, photos: this.album.photos}
    });
  }

}
