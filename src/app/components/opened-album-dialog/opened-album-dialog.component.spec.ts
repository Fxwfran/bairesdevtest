import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OpenedAlbumDialogComponent} from './opened-album-dialog.component';

describe('OpenedAlbumDialogComponent', () => {
  let component: OpenedAlbumDialogComponent;
  let fixture: ComponentFixture<OpenedAlbumDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OpenedAlbumDialogComponent]
    })
           .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenedAlbumDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
