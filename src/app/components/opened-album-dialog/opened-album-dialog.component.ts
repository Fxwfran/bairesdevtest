import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Photo} from "../../services/api.service";

@Component({
  selector: 'app-opened-album-dialog',
  templateUrl: './opened-album-dialog.component.html',
  styleUrls: ['./opened-album-dialog.component.css']
})
export class OpenedAlbumDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<OpenedAlbumDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { albumId: number, photos: Photo[] }) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
