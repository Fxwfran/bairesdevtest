import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


export interface Photo {
  albumId: number,
  id: number,
  title: string,
  url: string,
  thumbnailUrl: string,
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = "https://jsonplaceholder.typicode.com/photos";
  }

  listPhotos(): Observable<Photo[]> {
    return <Observable<Photo[]>>this.http.get(this.url);
  }
}
